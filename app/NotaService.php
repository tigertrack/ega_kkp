<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class NotaService extends Pivot
{
    protected $guarded = [];

    protected $table = "nota_service";

    public $timestamps = false;

    public function mekanik(){
        return $this->belongsTo('App\mekanik');
    }
}

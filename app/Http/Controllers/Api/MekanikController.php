<?php

namespace App\Http\Controllers\Api;

use App\mekanik;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MekanikController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return mekanik::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return mekanik::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\mekanik  $mekanik
     * @return \Illuminate\Http\Response
     */
    public function show(mekanik $mekanik)
    {
        return $mekanik;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\mekanik  $mekanik
     * @return \Illuminate\Http\Response
     */
    public function edit(mekanik $mekanik)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\mekanik  $mekanik
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, mekanik $mekanik)
    {
        return mekanik::where('id', $mekanik->id)->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\mekanik  $mekanik
     * @return \Illuminate\Http\Response
     */
    public function destroy(mekanik $mekanik)
    {
        $mekanik->delete();
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\pelanggan;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PelangganController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return pelanggan::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return pelanggan::create([
            'nama' => $request->nama,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'alamat' => $request->alamat,
            'telepon' => $request->telepon
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\pelanggan  $pelanggan
     * @return \Illuminate\Http\Response
     */
    public function show(pelanggan $pelanggan)
    {
        return $pelanggan;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\pelanggan  $pelanggan
     * @return \Illuminate\Http\Response
     */
    public function edit(pelanggan $pelanggan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\pelanggan  $pelanggan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, pelanggan $pelanggan)
    {
        pelanggan::where('id', $pelanggan->id)
            ->update($request->except('password'));
        return pelanggan::find($pelanggan);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\pelanggan  $pelanggan
     * @return \Illuminate\Http\Response
     */
    public function destroy(pelanggan $pelanggan)
    {
        $pelanggan->delete();
    }

    public function discount(pelanggan $pelanggan){
        $count = pelanggan::withCount('nota')->find($pelanggan->id);
        return $count->nota_count;
    }

    public function tracking(request $request){
        $pelanggan = pelanggan::with('nota.service')->where('telepon', 'like', "+%$request->telepon%")->first();

        foreach ($pelanggan->nota as $nota) {
            foreach ($nota->service as $service) {
                    $service->mekanik = \App\mekanik::find($service->pivot->mekanik_id);
            }
        }
        return $pelanggan;
    }
}

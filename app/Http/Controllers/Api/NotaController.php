<?php

namespace App\Http\Controllers\Api;

use App\nota;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NotaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return nota::with('admin', 'pelanggan', 'selesai', 'potongan')->orderBy('created_at', 'desc')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $nota = nota::create($request->except('details'));
        $total = 0;
        foreach ($request->details as $item) {
            $nota->service()->attach($item['service_id'], [
                'qty' => $item['qty'], 
                'mekanik_id' => $item['mekanik_id'],
                'price' => $item['biaya'],
                'keterangan' => $item['keterangan']
            ]);
            $total += $item['qty'] * $item['biaya'];
        }
        $nota->total = $total ;
        $nota->save();

        $pelanggan = \App\pelanggan::withCount('nota')->find($request->pelanggan_id);

        if($pelanggan->nota_count % 10 == 0){
            \App\potongan::create([
                'nota_id' => $nota->id,
                'nilai_potongan' => ($nota->total * 20/100 <= 75000 ? $nota->total * 20/100 : 75000)
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\nota  $nota
     * @return \Illuminate\Http\Response
     */
    public function show(nota $nota)
    {
        $nota = nota::with('admin', 'pelanggan', 'service')->find($nota->id);
        
        foreach ($nota->service as $service) {
            
            $service->mekanik = \App\mekanik::find($service->pivot->mekanik_id);
        }
        return $nota;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\nota  $nota
     * @return \Illuminate\Http\Response
     */
    public function edit(nota $nota)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\nota  $nota
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, nota $nota)
    {
        return response('', 403);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\nota  $nota
     * @return \Illuminate\Http\Response
     */
    public function destroy(nota $nota)
    {
        return response('', 403);
    }
}

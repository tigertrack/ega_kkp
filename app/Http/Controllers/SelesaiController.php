<?php

namespace App\Http\Controllers;

use App\selesai;
use Illuminate\Http\Request;

class SelesaiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\selesai  $selesai
     * @return \Illuminate\Http\Response
     */
    public function show(selesai $selesai)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\selesai  $selesai
     * @return \Illuminate\Http\Response
     */
    public function edit(selesai $selesai)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\selesai  $selesai
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, selesai $selesai)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\selesai  $selesai
     * @return \Illuminate\Http\Response
     */
    public function destroy(selesai $selesai)
    {
        //
    }
}

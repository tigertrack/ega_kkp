<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class pelanggan extends Model
{
    protected $guarded = [];

    protected $hidden = ["password"];
    
    public function nota(){
        return $this->hasMany('App\nota');
    }
}

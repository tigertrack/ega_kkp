<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class nota extends Model
{
    protected $guarded = [];

    

    public function service(){
        return $this->belongsToMany('App\service') 
            ->using('App\NotaService')
            ->withPivot(['qty', 'mekanik_id', 'price', 'status', 'keterangan']);;
    }

    public function admin(){
        return $this->belongsTo('App\admin');
    }

    public function pelanggan(){
        return $this->belongsTo('App\pelanggan');
    }

    public function selesai(){
        return $this->hasOne('App\Selesai');
    }

    public function potongan(){
        return $this->hasOne('App\potongan');
    }
}

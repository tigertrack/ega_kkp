<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
        <link rel=stylesheet href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900">
        <link rel=stylesheet href="https://fonts.googleapis.com/css?family=Material+Icons">
        <link href=/js/app.js rel=preload as=script>
    </head>
    <body>
        <div id=app></div>
        <script src=/js/app.js></script>
    </body>
</html>

@extends('reports.template')

@section('title')
LAPORAN TRANSAKSI PERIODIK
@endsection

@section('content')
    <div class="row">
        <div class="col-sm">
            <table class="table table-bordered table-sm">
                <thead>
                    <tr>
                        <th>Nama Pelanggan</th>
                        <th>Tanggal Transaksi</th>
                        <th>Nama Admin</th>
                        <th>Total Biaya</th>
                        <th>Potongan</th>
                        <th>Status</th>
                        
                    </tr>
                </thead>
                <tbody>
                    @foreach($data as $row)
                    <tr>
                        <td>{{$row->pelanggan->nama}}</td>
                        <td>{{\Carbon\Carbon::parse($row->created_at)->format('j F Y')}}</td>
                        <td>{{$row->admin->nama}}</td>
                        <td>{{$row->total}}</td>
                        <td>
                            @if($row->potongan)
                            {{$row->potongan->nilai_potongan}}
                            @else
                            Tidak ada
                            @endif
                        </td>
                        <td>
                            @if($row->selesai)
                            Selesai
                            @else
                            Belum Selesai
                            @endif
                        </td>
                       
                    </tr>
                    @endforeach
                    
                </tbody>
            </table>
        </div>
    </div>
@endsection

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> -->
    <style>
    /** 
                Set the margins of the page to 0, so the footer and the header
                can be of the full height and width !
             **/
            @page {
                margin: 0cm 0cm;
            }

            /** Define now the real margins of every page in the PDF **/
            body {
                
                margin-left: 1cm;
                margin-right: 1cm;
                margin-bottom: 2cm;
                font-family : 'Helvetica';
            }

            /** Define the header rules **/
            header {
                font-size : 10pt;
                position: fixed;
                top: 0cm;
                left: 0cm;
                right: 0cm;
                padding-right: 16px;

                /** Extra personal styles **/
                background-color: #03a9f4;
                color: white;
                text-align: right;
                /* line-height: .5cm; */
            }

            /** Define the footer rules **/
            footer {
                position: fixed; 
                bottom: 0cm; 
                left: 0cm; 
                right: 0cm;
                height: 2cm;

                /** Extra personal styles **/
                background-color: #03a9f4;
                color: white;
                text-align: center;
                line-height: 1.5cm;
            }
        table{
            font-size : 10pt;
            border-collapse : collapse;
            width : 100%;
        }

        td, th, table {
            border : 1px solid;
        }

        td, th {
            padding : 5px 15px;
        }

        .headline {
            text-align: center;   
        }

        .headline> h2 {
            margin-bottom : 0;
        }

        .headline> p {
            margin-top : 0;
        }
    </style>
    <title>@yield('title')</title>
  </head>
  <body>
    
    <div class="container-fluid">
        <div class="header">
            
            <div class="headline">
                <h3>@yield('title')</h3>

                
            </div>
                <p>Tanggal Cetak : <br> periode : 
                {{ $start ? \Carbon\Carbon::parse($start)->format('d/m/Y') : ''}} - {{ $end ? \Carbon\Carbon::parse($end)->format('d/m/Y') : ''}}</p>

        </div>
            
        @yield('content')
            
    </div>
    
  </body>
</html>

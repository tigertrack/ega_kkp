<?php

use Illuminate\Http\Request;
use Carbon\carbon;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:api'], function(){
    
    Route::resources([
        'services' => 'Api\ServiceController',
        'mekaniks' => 'Api\MekanikController',
        'pelanggans' => 'Api\PelangganController',
        'admins' => 'Api\AdminController',
        'notas' => 'Api\NotaController'
    ]);
    
    Route::get('/ongoing', function(){
        return App\nota::doesntHave('selesai')->count();
    });
    
    Route::get('/finished', function(){
        return App\nota::has('selesai')->count();
    });
    
    Route::get('/pelanggans/{pelanggan}/discount', 'Api\PelangganController@discount')->name('pelanggan.discount');
    
    Route::post('/tracking', 'Api\PelangganController@tracking')->name('pelanggan.tracking');
    
    Route::post('/selesai', function(request $request){
        \App\NotaService::where('nota_id', $request->nota_id)
            ->where('service_id', $request->service_id)
            ->update([
                'status' => true
            ]);
    });
    
    Route::post('/cetak', function(request $request){
        $start = Carbon::createFromFormat('d/m/Y', $request->start);
        $end = Carbon::createFromFormat('d/m/Y', $request->end);
        $data = App\nota::with('admin', 'pelanggan', 'selesai', 'potongan')
        ->whereBetween('created_at', [$start, $end])
        ->orderBy('created_at', 'asc')->get();
        $pdf = PDF::loadView('reports.transaksi', compact('data', 'start', 'end'));
        return $pdf->download('invoice.pdf');
    });  
});

Route::post('login', 'Api\AuthController@login');


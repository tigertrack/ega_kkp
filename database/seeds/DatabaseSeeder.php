<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        factory(App\admin::class, 5)->create();

        factory(App\pelanggan::class, 50)->create();

        factory(App\mekanik::class, 8)->create();

        factory(App\service::class, 100)->create();

        factory(App\nota::class, 100)->create()->each(function ($nota){
            $services = App\service::all()->random(rand(1,10));
            $total = 0;
            foreach ($services as $service) {
                $qty = 1;
                $price =rand($service->min, $service->max);
                $nota->service()->attach($service->id, [
                    'qty' => $qty, 
                    'mekanik_id' => App\mekanik::all()->random()->id,
                    'price' => $price
                ]);
                $total += $qty * $price;
            }

            $nota->total = $total;

            $nota->save();

            if(rand(0,1) >= 1)
                $nota->selesai()->save(new App\selesai);
        });

        $pelanggans = App\pelanggan::all();
        foreach ($pelanggans as $pelanggan) {
            $counter = 0;
            foreach ($pelanggan->nota as $nota) {
                if($counter % 10 == 0){
                    App\potongan::create([
                        'nota_id' => $nota->id,
                        'nilai_potongan' => $nilai_potongan = ($nota->total * 20/100 <= 75000 ? $nota->total * 20/100 : 75000)
                    ]);
                }
                $counter++;
            }
        }
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotaServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nota_service', function (Blueprint $table) {
            $table->integer('nota_id');
            $table->integer('service_id'); 
            $table->integer('mekanik_id')->nullable(); 
            $table->smallInteger('qty')->nullable();
            $table->double('price', 8, 2);
            $table->boolean('status')->default(false);
            $table->string('keterangan')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nota_service');
    }
}

<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\service;
use Faker\Generator as Faker;

$factory->define(service::class, function (Faker $faker) {
    $a = $faker->numberBetween($min = 50000, $max = 900000);
    $b = $faker->numberBetween($min = 50000, $max = 900000);
    return [
        'nama' => $faker->name,
        'min' => min($a, $b),
        'max' => max($a, $b)
    ];
});

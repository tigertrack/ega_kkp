<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\mekanik;
use Faker\Generator as Faker;

$factory->define(mekanik::class, function (Faker $faker) {
    return [
        'nama' => $faker->name,
        'telepon' => $faker->e164PhoneNumber    
    ];
});

<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\nota;
use App\pelanggan;
use App\admin;
use Faker\Generator as Faker;

$factory->define(nota::class, function (Faker $faker) {
    $rand_date = $faker->dateTimeBetween($startDate = '-1 years', $endDate = 'now', $timezone = null);
    return [
        'admin_id' => admin::all()->random()->id,
        'pelanggan_id' => pelanggan::all()->random()->id,
        'created_at' => $rand_date,
        'updated_at' => $rand_date
    ];
});

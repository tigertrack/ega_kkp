<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\pelanggan;
use Faker\Generator as Faker;

$factory->define(pelanggan::class, function (Faker $faker) {
    return [
        'nama' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'alamat' => $faker->address,
        'telepon' => $faker->e164PhoneNumber
    ];
});
